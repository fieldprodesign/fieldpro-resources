const path = require(`path`)

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions

  const blogPost = path.resolve(`./src/templates/blogPost.js`)
  const blogCategory = path.resolve(`./src/templates/category.js`)

  
  const workflowPage = path.resolve(`./src/templates/workflow-page.js`)
  const addOnPage = path.resolve(`./src/templates/addOn-page.js`)


  return graphql(
    `
      {
        allContentfulFeaturesPage {
          edges {
            node {
              id
              slug
              pageTitleAsSeenOnMenu
            }
          }
        }
        allContentfulAddOnPage {
          edges {
            node {
              title
              slug
              id
            }
          }
        }
        allContentfulWorkflowTemplate {
          edges {
            node {
              title
              slug
              id
            }
          }
        }
        allContentfulBlogPost {
          edges {
            node {
              slug
              title
              language
            }
          }
        }
        allContentfulBlogCategory {
          edges {
            node {
              language
              slug
              title
            }
          }
        }
        allContentfulIndustryPage {
          edges {
            node {
              id
              slug
              pageName
            }
          }
        }
        allContentfulProductPage {
          edges {
            node {
              id
              slug
              pageName
            }
          }
        }
      }
    `
  ).then(result => {
    if (result.errors) {
      throw result.errors
    }
  
    const posts = result.data.allContentfulBlogPost.edges
    const categories = result.data.allContentfulBlogCategory.edges
    const workflow = result.data.allContentfulWorkflowTemplate.edges
    const addOn = result.data.allContentfulAddOnPage.edges
    // const partner = result.data.allContentfulFindPartnerPage.edges

    
      //create blog posts page
      posts.forEach((post, index) => {
        const previous =
          index === posts.length - 1 ? null : posts[index + 1].node
        const next = index === 0 ? null : posts[index - 1].node

        createPage({
          path: post.node.slug,
          component: blogPost,
          context: {
            slug: post.node.slug,
            language: post.node.language,
            previous,
            next,
          },
        })
      })
    //create categories page
    categories.forEach((category, index) => {
      const previous =
        index === categories.length - 1 ? null : categories[index + 1].node
      const next = index === 0 ? null : categories[index - 1].node

      createPage({
        path: category.node.slug,
        component: blogCategory,
        context: {
          slug: category.node.slug,
          previous,
          next,
        },
      })
    })
 
    // Create workflow pages.

    workflow.forEach((workflowTemp, index) => {
      const previous =
        index === workflow.length - 1 ? null : workflow[index + 1].node
      const next = index === 0 ? null : workflow[index - 1].node

      createPage({
        path: workflowTemp.node.slug,
        component: workflowPage,
        context: {
          slug: workflowTemp.node.slug,
          previous,
          next,
        },
      })
    })
    // Create  addOn pages.

    addOn.forEach((addOnTemp, index) => {
      const previous = index === addOn.length - 1 ? null : addOn[index + 1].node
      const next = index === 0 ? null : addOn[index - 1].node

      createPage({
        path: addOnTemp.node.slug,
        component: addOnPage,
        context: {
          slug: addOnTemp.node.slug,
          previous,
          next,
        },
      })
    })

   

   
  })
}
