import React from "react"
import { Link, graphql } from "gatsby"
import styled from "styled-components"
import { BLOCKS, INLINES } from "@contentful/rich-text-types"
import { documentToReactComponents } from "@contentful/rich-text-react-renderer"

import { ButtonChip } from "../components/buttons/CtaButton.js"
import Layout from "../components/layout.js"
import Seo from "../components/seo.js"

import NewHeader from "../components/navigation/HeaderNew.js"
import NewHeaderES from "../components/navigation/HeaderNewES.js"
import NewHeaderFR from "../components/navigation/HeaderNewFR.js"
import Footer from "../components/navigation/Footer.js"
import FooterFR from "../components/navigation/FooterFR.js"
import FooterES from "../components/navigation/FooterES.js"
import ScrollToTop from "../components/sections/ScrollToTop.js"
import { H1, H2, H3, P, BlogText, H4, H5 } from "../components/styles/TextStyles.js"


import LightCta from "../components/calltoaction/LightCta.js"
import LightCtaFR from "../components/calltoaction/LightCtaFR.js"
import LightCtaES from "../components/calltoaction/LightCtaES.js"
import DarkCtaFR from "../components/calltoaction/DarkCtaFR.js"
import DarkCtaES from "../components/calltoaction/DarkCtaES.js"
import DarkCta from "../components/calltoaction/DarkCta.js"

class AddOnsPage extends React.Component {
  render() {
    const workflow = this.props.data.contentfulAddOnPage
    const cards = this.props.data.allContentfulAddOnPage.edges

    const filteredArray = cards.filter(cardoption => {
      return (
        workflow.language === cardoption.node.language &&
        workflow.title !== cardoption.node.title
      )
    })
    const _ = require("lodash")

    let shuffledArray = _.sampleSize(filteredArray, 3)

    const options = {
      renderNode: {
        [BLOCKS.HEADING_1]: (node, children) => <H1>{children}</H1>,
        [BLOCKS.HEADING_2]: (node, children) => <H2>{children}</H2>,
        [BLOCKS.HEADING_3]: (node, children) => <H3>{children}</H3>,
        [BLOCKS.PARAGRAPH]: (node, children) => <BlogText>{children}</BlogText>,
        [BLOCKS.EMBEDDED_ENTRY]: (node, children) => (
          <Link to={node.data.uri} target="_blank">
            {children}
          </Link>
        ),
        [BLOCKS.EMBEDDED_ASSET]: (node) => {
          const { target } = node.data;
          const assetId = target.sys.id;
          const foundAsset = this.props.data.allContentfulAsset.nodes.find((node) => node.contentful_id === assetId);
        
          if (foundAsset) {
            const { title, description, file } = foundAsset;
            const imageUrl = file.url;
            const imageAlt = title || description || '';
        
            return <img src={imageUrl} alt={imageAlt} />;
          }
        
          return null;
        },
        [INLINES.EMBEDDED_ENTRY]: (node, children) => (
          <Link to={node.data.uri} target="_blank">
            {children}
          </Link>
        ),
        [INLINES.HYPERLINK]: (node, children) => {
          if (node.data.uri.includes("player.vimeo.com/video")) {
            return (
              <IframeContainer>
                <iframe
                  title="Unique Title 001"
                  src={node.data.uri}
                  frameBorder="0"
                  allowFullScreen
                ></iframe>
              </IframeContainer>
            )
          } else if (node.data.uri.includes("youtube.com/embed")) {
            return (
              <IframeContainer>
                <iframe
                  title="Unique Title 002"
                  src={node.data.uri}
                  allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"
                  frameBorder="0"
                  allowFullScreen
                ></iframe>
              </IframeContainer>
            )
          } else {
            return (
              <Link to={node.data.uri} target="_blank">
                {children}
              </Link>
            )
          }
        },

        [INLINES.ENTRY_HYPERLINK]: (node, children) => (
          <Link to={node.data.uri} target="_blank">
            {children}
          </Link>
        ),
        [INLINES.ASSET_HYPERLINK]: (node, children) => (
          <Link to={node.data.uri} target="_blank">
            {children}
          </Link>
        ),
      },
      renderMark: {},
    }

    var headerMenu
    if (workflow.language === "EN") {
      headerMenu = (
        <NewHeader
          en={`/${workflow.slug}`}
          fr={`/${workflow.slug}-fr`}
          es={`/${workflow.slug}-es`}
        />
      )
    }
    if (workflow.language === "FR") {
      var currentSlug = workflow.slug
      currentSlug = currentSlug.substring(0, currentSlug.length - 3)
      headerMenu = (
        <NewHeaderFR
          en={`/${currentSlug}`}
          fr={`/${currentSlug}-fr`}
          es={`/${currentSlug}-es`}
        />
      )
    }
    if (workflow.language === "ES") {
      var currentSlugEs = workflow.slug
      currentSlugEs = currentSlugEs.substring(0, currentSlugEs.length - 3)
      headerMenu = (
        <NewHeaderES
          en={`/${currentSlugEs}`}
          fr={`/${currentSlugEs}-fr`}
          es={`/${currentSlugEs}-es`}
        />
      )
    }

    var backLink
    if (workflow.language === "EN") {
      backLink = (
        <Link to="/add-ons">
          <BackGroup>
            <BackIcon
              src="/images/Icons/arrow_back-24px.svg"
            />
            <BackLabel>Back to All Add ons</BackLabel>
          </BackGroup>
        </Link>
      )
    }
    if (workflow.language === "FR") {
      backLink = (
        <Link to="/add-ons-fr">
          <BackGroup>
            <BackIcon
              src="/images/Icons/arrow_back-24px.svg"
            />
            <BackLabel>Retour à tous les add-ons</BackLabel>
          </BackGroup>
        </Link>
      )
    }
    if (workflow.language === "ES") {
      backLink = (
        <Link to="/add-ons-es">
          <BackGroup>
            <BackIcon
              src="/images/Icons/arrow_back-24px.svg"
            />
            <BackLabel>Volver a todas las add-ons</BackLabel>
          </BackGroup>
        </Link>
      )
    }

    //Modal
    var ctaModal
    if (workflow.language === "EN") {
      ctaModal = (
       
          <LightCta />
       
      )
    }
    if (workflow.language === "FR") {
      ctaModal = (
       
          <LightCtaFR />
      
      )
    }
    if (workflow.language === "ES") {
      ctaModal = (
   
          <LightCtaES />
    
      )
    }

    //BottomModal
    var bottomCtaModal
    if (workflow.language === "EN") {
      bottomCtaModal = (
      
          <DarkCta />
       
      )
    }
    if (workflow.language === "FR") {
      bottomCtaModal = (
   
          <DarkCtaFR />
    
      )
    }
    if (workflow.language === "ES") {
      bottomCtaModal = (
   
          <DarkCtaES />
    
      )
    }

    var footerMenu
    if (workflow.language === "EN") {
      footerMenu = <Footer />
    }
    if (workflow.language === "FR") {
      footerMenu = <FooterFR />
    }
    if (workflow.language === "ES") {
      footerMenu = <FooterES />
    }
    // //Background image for testimonials section
    // var background = workflow.testimonialBackgroundImage.file.url
    return (
      <Layout location={this.props.location}>
        <Seo
          title={workflow.metaTitle}
          description={workflow.metaDescription}
          //   mainImage={`https:${workflow.image.file.url}`}
        />
        {headerMenu}
        <HeaderSectionWrapper>
     <BreadCrumbContainer>{backLink} </BreadCrumbContainer>

     <HeaderContent>
        <HeaderIcon>          
        <img src={workflow.integrationIcon.file.url} alt="" />
</HeaderIcon>
        <HeadLine>{workflow.title}</HeadLine>
        <HeaderDetails>{workflow.metaDescription}</HeaderDetails>
        {ctaModal}
     </HeaderContent>
    </HeaderSectionWrapper>
        <BodySectionWrapper>
    <Aside><Title2>Categories</Title2>
            <TagsGroup>
              {this.props.data.contentfulAddOnPage.tags.map(wftemplatetags => {
                return <ButtonChip>{wftemplatetags.title}</ButtonChip>
              })}
            </TagsGroup></Aside>
    <Main> {documentToReactComponents(
                    JSON.parse(workflow.content.raw, options)
                    )}</Main>
    </BodySectionWrapper>
       

        <MoreTemplatesWrapper>
          <MoreSectionTitle>
            {workflow.integrationSectionTitle}
          </MoreSectionTitle>
          <WorkflowTemplatesWrapper>
            {shuffledArray.map(wfCards => {
              // if (
              //   workflow.language === wfCards.node.language &&
              //   workflow.title !== wfCards.node.title
              // ) {
              return (
                <Link to={`/${wfCards.node.slug}`} key={wfCards.node.id}>
                  <CardTemplateWrapper>
                    <CardTemplateIcon>
                      {
                        <img
                          src={wfCards.node.integrationIcon.file.url}
                          alt=""
                        />
                      }
                    </CardTemplateIcon>
                    <CardTemplateTitle>{wfCards.node.title}</CardTemplateTitle>
                    <CardTemplateDescription>
                      {wfCards.node.metaDescription}
                    </CardTemplateDescription>
                    <CardDividerLine></CardDividerLine>
                    <CardChipGroup>
                      {wfCards.node.tags.map(templatetags => {
                        return <ButtonChip>{templatetags.title}</ButtonChip>
                      })}
                    </CardChipGroup>
                  </CardTemplateWrapper>
                </Link>
              )
              // }
              // return null
            })}
          </WorkflowTemplatesWrapper>
        </MoreTemplatesWrapper>

        <BottomCallToAction>
          <CtaBackground>
            <img
              style={{ width: "100%" }}
              src="/images/Geo-location.svg"
              alt="Illustration"
            />
          </CtaBackground>
          <CtaContent>
            <CtaText>
              <Tagline style={{ color: "#fefefe" }}>
                {workflow.callToAction}
              </Tagline>
              <CtaGroup>{bottomCtaModal}</CtaGroup>
            </CtaText>
          </CtaContent>
        </BottomCallToAction>
        {footerMenu}
        <ScrollToTop />
      </Layout>
    )
  }
}

export default AddOnsPage

export const integrationPostQuery = graphql`
  query integrationPostBySlug($slug: String!) {
    site {
      siteMetadata {
        title
        siteUrl
      }
    }
    allContentfulAddOnPage {
      edges {
        node {
          title
          language
          slug
          metaDescription
          integrationIcon {
            file {
              url
            }
          }
          tags {
            title
          }
        }
      }
    }
    contentfulAddOnPage(slug: { eq: $slug }) {
      metaTitle
      metaDescription
      language
      slug
      title
      integrationIcon {
        file {
          url
        }
      }
      content {
        raw
      }

      callToAction

      integrationSectionTitle
      tags {
        title
      }
    }
    allContentfulAsset {
      nodes {
        contentful_id
        title
        description
        file {
          url
        }
      }
    }
  }
`




const TagsGroup = styled.div`
  padding: 0;
  margin: 0;
  display: flex;
  flex-direction: row;
  grid-gap: 20px;
  @media (max-width: 830px) {
    margin: 40px 32px;
  }
  @media (max-width: 512px) {
    margin: 32px 16px;
  }
`

const MoreTemplatesWrapper = styled.div`
  max-width: 1280px;
  margin: 60px auto 120px auto;
  @media (max-width: 830px) {
    max-width: 720px;
    margin: 40px auto 40px auto;
  }
  @media (max-width: 512px) {
    max-width: 350px;
    margin: 40px auto 40px auto;
  }
`
const MoreSectionTitle = styled(H3)`
  @media (max-width: 830px) {
    max-width: 720px;
    margin: 0px 14px 80px 16px;
  }
  @media (max-width: 512px) {
    max-width: 350px;
    margin: 0px 14px 60px 14px;
  }
`



const Image = styled.div``


//Youtube video embed
const IframeContainer = styled.span`
  padding-bottom: 56.25%;
  position: relative;
  display: block;
  width: 100%;

  > iframe {
    height: 100%;
    width: 100%;
    position: absolute;
    top: 0;
    left: 0;
  }
`
//Share buttons styling

/// MORE TEMPLATES SECTION
const WorkflowTemplatesWrapper = styled.div`
  max-width: 1280px;
  margin: 60px auto 120px auto;
  display: grid;
  grid-template-columns: repeat(3, auto);
  gap: 40px;
  @media (max-width: 830px) {
    max-width: 720px;
    margin: 0;
    padding: 0;
    grid-template-columns: repeat(2, auto);
    gap: 30px;
  }
  @media (max-width: 512px) {
    max-width: 320px;
    margin: 0;
    padding: 0;
    grid-template-columns: repeat(1, auto);
    gap: 30px;
  }
`

//CARDS CSS
const CardTemplateWrapper = styled.div`
  width: 400px;
  height: 420px;
  border: 1px solid #979797;
  background-color:#fefefe;
  border-radius: 5px;
  padding: 40px;
  &:hover,
  &:focus {
    background-color: #e7edef;
    border:none;
    box-shadow: 0 0.5em 1em #124e5d07;
  }
  @media (max-width: 830px) {
    padding: 20px;
    width: 320px;
  }
  @media (max-width: 512px) {
    padding: 20px;
    width: 320px;
  }
`
const CardTemplateIcon = styled.div`
  height: 54px;
  width: 54px;
`

const Title2 = styled(H5)`
margin-top:48px;
color: #6c6c6c;
  @media (max-width: 830px) {
    margin: 24px 32px;
  }
  @media (max-width: 512px) {
    margin: 24px 16px;
  }
`



const CardTemplateTitle = styled(H4)`
  margin: 20px 0 10px 0;
`
const CardTemplateDescription = styled(P)``
const CardDividerLine = styled.div`
  width: 320px;
  height: 1px;
  background: #d8d8d8;
  padding: 0;
  margin: 40px 0 20px 0;
  @media (max-width: 830px) {
    width: 280px;
  }
  @media (max-width: 512px) {
    width: 280px;
  }
`
const CardChipGroup = styled.div`
  display: flex;
  flex-direction: row;
  grid-gap: 20px;
`



const BottomCallToAction = styled.div`
  position: relative;
  display: grid;
  background-color: #124e5d;
  width: 100vw;
  left: -8px;
  margin: 200px 0 280px 0px;
  padding: 0;
  @media (max-width: 830px) {
    width: 100vw;
    left: 0;
    margin: 80px 0 0 0px;
  }
  @media (max-width: 450px) {
    width: 370px !important;
    margin: 100px 0px;
    left: 0;
  }
`
const CtaBackground = styled.div`
  position: absolute;
  z-index: 1;
  align-self: center;
  width: 100%;
`

const CtaContent = styled.div`
  position: relative;
  display: grid;
  max-width: 72em;
  margin: 120px 0 160px 280px;
  box-shadow: 0 0.5em 2em #2c2c2c18;

  padding: 0;
  @media (max-width: 830px) {
    margin: 80px 40px 40px 40px;
    padding: 0;
  }
  @media (max-width: 450px) {
    width: 320px;
    margin: 40px 16px 40px 16px;
  }
`
const Tagline = styled(H2)`
  padding: 10px 0px 0px 0px;
  max-width: 900px;
  @media (max-width: 830px) {
    margin: 0;
    padding: 10px 0px 40px 0px;
    width: auto;
  }
  @media (max-width: 450px) {
    padding: 10px 0px 40px 0px;
    width: auto;
  }
`
const CtaText = styled.div`
  position: relative;
  align-self: center;

  color: #2a2a2a;
  max-width: 56em;

  z-index: 2;
  h2 {
    font-size: 3.25em;
  }
  @media (max-width: 32em) {
    width: 24em;
    margin: 0;
    padding: 0;
    h2 {
      font-size: 2.5em;
      margin: 1.2em 1.2em 1em 0;
    }
  }
  @media (max-width: 450px) {
    max-width: 360px;
    margin: 0;
    padding: 0;
  }
`

const CtaGroup = styled.div`
  margin: -20px 0 0 0;
  max-width: 640px;
  display: flex;
  flex-direction: row;
  grid-gap: 20px;
  @media (max-width: 450px) {
    flex-direction: column;
  }
`


//////hero content/////////
const HeaderSectionWrapper = styled.div`
max-width: 1280px;
margin:120px auto;

width: calc(100% - (4 * 32px));
@media (max-width: 512px) {
    margin:64px auto;
  }

`
const BreadCrumbContainer=styled.div`
max-width:1280px;


`
const BackGroup = styled.div`
  display: grid;
  grid-template-columns: repeat(2, auto);
  justify-content: flex-start;
  width: 60em;
  margin: 0;
  padding: 0;
  :hover {
   color: #febd55;
  }
  @media (max-width: 830px) {
    width: 520px;
    padding: 0 32px;
  }
  @media (max-width: 512px) {
    padding: 0;
    width: 320px;
  }
`
const BackIcon = styled.img`
  align-self: center;
  height: 32px;
  width: 32px;
  margin: 0 0.4em 0 0;
  padding: 0;
`
const BackLabel = styled.div`
  align-self: center;
  font-weight: 600;
  font-size: 16px;
  color: #6c6c6c;
  margin: 0;
  padding: 0;
`

const HeaderContent=styled.div`
margin:0 auto;
max-width: 800px;
display:flex;
flex-direction: column; 
  justify-content: center; 
  align-items:center;
`
const HeaderIcon=styled.div`
max-width: 120px;
`
const HeaderDetails=styled(P)`
text-align:center;
max-width: 600px;
`
const HeadLine=styled(H1)`text-align:center;`
//////Bodycontent///////

const BodySectionWrapper = styled.div`
max-width: 1280px;
margin:0 auto;

width: calc(100% - (4 * 32px));
display:flex;
justify-content:center;
`
const Aside=styled.nav`
flex:1;
max-width:150px;
margin-right:40px;

@media (max-width: 700px) {
    display: none;
    }


`
const Main=styled.main`
flex:3;
max-width:800px;

  padding: 0;
  img {
    margin: 1em 1em 1em 0;
    padding: 0;
    width: 39em;
    max-height: 40em;
    object-fit: contain;
  }
  a {
    font-size: 1em;
    font-weight: 600;
    line-height: 1.5;
    color: #febd55;
    text-decoration: none;
    cursor: pointer;
    margin: 0;
    padding: 0.1em;
    :hover {
      font-weight: 700;
      border-bottom: none;
      padding-bottom: 0;
    }
  }
  @media (max-width: 830px) {
    margin: 0 32px;
  }
  @media (max-width: 512px) {
    margin: 0 1em 2em 1em;
    width: 19em;
    img {
      margin: 1em 1em 1em 0;
      width: 18.5em;
    }
    p,
    a {
      margin: 0 1em 1em 0;
      width: 15em;
    }
    h2,
    h3,
    h4,
    h5,
    h6 {
      margin: 1em 1em 1em 0;
      width: 10em;
    }
    ul,
    li {
      margin: 0 10em 0 0.5em !important;
      width: 12em !important;
    }
  }`
