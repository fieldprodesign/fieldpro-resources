import React from "react"
import { useEffect, useState } from "react"

import { RoundButtonUp } from "../buttons/CtaButton.js"

const ScrollToTop = () => {
  // The back-to-top button is hidden at the beginning
  const [showButton, setShowButton] = useState(false)

  useEffect(() => {
    window.addEventListener("scroll", () => {
      if (window.pageYOffset > 300) {
        setShowButton(true)
      } else {
        setShowButton(false)
      }
    })
  }, [])

  // This function will scroll the window to the top
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth", // for smoothly scrolling
    })
  }

  return (
    <>{showButton && <RoundButtonUp onClick={scrollToTop}></RoundButtonUp>}</>
  )
}

export default ScrollToTop
