import {
    BodyMain,
    Caption,
    H1,
    H2,
    H4,
    P,
  } from "./TextStyles.js"
  import styled from "styled-components"
export const HeroSectionWrapper = styled.div`
  max-width: 1280px;
  margin: 0 auto;
`
export const HeroSection = styled.div`
  padding: 240px 0px 0px 0px;
  max-width: 800px;

  @media (max-width: 830px) {
    padding: 120px 40px 40px 40px;
    margin: 0 0 0 0px;
    max-width: 672px;
  }
  @media (max-width: 512px) {
    padding: 80px 24px 40px 24px;
    margin: 0 0 0 0px;
    max-width: 360px;
  }
`
export const HeroTitle = styled(H1)`
  margin: 0 0 16px 0;
`
export const HeroDescription = styled(BodyMain)``
export const WorkflowTemplatesWrapper = styled.div`
  max-width: 1280px;
  margin: 60px auto 120px auto;
  display: grid;
  grid-template-columns: repeat(3, auto);
  gap: 40px;
  @media (max-width: 830px) {
    padding: 40px 40px 80px 40px;
    grid-template-columns: repeat(2, auto);
    gap: 30px;
  }
  @media (max-width: 512px) {
    margin: 60px auto 40px auto;
    padding: 40px 24px 40px 24px;
    grid-template-columns: repeat(1, auto);
    gap: 30px;
  }
`

export const FilterSearchBar = styled.div`
 max-width: 1280px;
  margin: 0 auto;
  display: flex;
 justify-content:flex-start;
`
export const FilterBar = styled.div``

//SEARCH BAR
export const SeearchBarContainer = styled.div`
  width: 480px;
 
  @media (max-width: 830px) {
    margin: 20px 0 0 28px;
    width: 640px;
  }
  @media (max-width: 512px) {
    width: 320px;
    margin: 0px 0 0 28px;
  }
`
export const SearchInput = styled.input`
  width: 480px;
  height: 64px;
  margin: 0 0px 0 0px;

  border-radius: 8px;
  border: solid 1px #d3d3d3;
  font-size: 16px;
  padding: 15px 45px 15px 15px;

  color: #6c6c6c;
  border-radius: 6px;

  transition: all 0.4s;
  :focus {
    border: solid 1px #febd55;
    outline: none;
    box-shadow: 0 1px 8px #b8c6db;
    -moz-box-shadow: 0 1px 8px #b8c6db;
    -webkit-box-shadow: 0 1px 8px #b8c6db;
  }
  @media (max-width: 830px) {
    margin: 0;
    width: 640px;
  }
  @media (max-width: 512px) {
    width: 320px;
    margin: 0;
  }
`

//CARDS CSS
export const CardTemplateWrapper = styled.div`
 width: 400px;
  height: 420px;
  border: 1px solid #979797;
  background-color:#fefefe;
  border-radius: 5px;
  padding: 40px;
  &:hover,
  &:focus {
    background-color: #e7edef;
    border:none;
    box-shadow: 0 0.5em 1em #124e5d07;
  }
  @media (max-width: 830px) {
    padding: 20px;
    width: 320px;
    height: auto;
  }
  @media (max-width: 512px) {
    padding: 20px;
    width: 320px;
    height: auto;
  }
`
export const CardTemplateIcon = styled.div`
  height: 54px;
  width: 54px;
`
export const CardTemplateTitle = styled(H4)`
  margin: 20px 0 10px 0;
`
export const CardTemplateDescription = styled(P)``
export const CardDividerLine = styled.div`
  width: 320px;
  height: 1px;
  background: #d8d8d8;
  padding: 0;
  margin: 40px 0 20px 0;
  @media (max-width: 830px) {
    width: 280px;
  }
  @media (max-width: 512px) {
    width: 280px;
  }
`
export const CardChipGroup = styled.div`
  display: flex;
  flex-direction: row;
  grid-gap: 20px;
`

//CTA TEXT
export const CtaBackground = styled.div`
  background-color: #124e5d;
  width: 100vw;
  position: relative;
  left: -8px;
`
export const CtaWrapper = styled.div`
  max-width: 1280px;
  margin: 0 auto;
  padding: 200px 0 200px 0;
  @media (max-width: 830px) {
    padding: 100px 48px 100px 48px;
  }
  @media (max-width: 512px) {
    padding: 80px 24px 80px 24px;
  }
`
export const CtaIllustartion = styled.div`
  position: absolute;
  align-self: center;
  top: 100px;
`
export const CtaContent = styled.div`
  position: relative;
  z-index: 1;
  max-width: 1280px;
  padding: 0 0 0 0;
  @media (max-width: 830px) {
    padding: 0;
  }
  @media (max-width: 512px) {
    padding: 0px;
  }
`
export const CtaTagline = styled(H2)`
  font-size: 3.25em;
  color: #fefefe;
  max-width: 600px;
  margin: 0 0 20px 0;
  @media (max-width: 830px) {
    padding: 0px;
  }
  @media (max-width: 512px) {
    padding: 0px;
    max-width: 320px;
    font-size: 2.5em;
    margin: 0 0 40px 0;
  }
`
export const CtaButtons = styled.div`
  display: flex;
  flex-direction: row;
  grid-gap: 20px;
  @media (max-width: 512px) {
    flex-direction: column;
  }
`
export const TrialGroup = styled.div``
export const DisclaimerText = styled(Caption)`
  color: #fefefe;
  padding: 4px 0 0 16px;
  font-weight: 600;
  font-size: 14px;
`
