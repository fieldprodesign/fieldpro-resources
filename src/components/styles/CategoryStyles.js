import styled from "styled-components"
export const Body = styled.div`
  display: grid;
  justify-items: center;
  margin: 8em 0 0 0;
  padding: 0;
  @media (max-width: 32em) {
    margin: 10em 0 0 0;
  }
`
