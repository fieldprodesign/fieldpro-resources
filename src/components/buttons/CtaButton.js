import styled from "styled-components"
export const ScheduleButton = styled.button`
  color: #febd55;
  border-color: #febd5590;
  border-width: 0.15em;
  border-style: solid;
  border-radius: 0.2em;
  background: none;
  cursor: pointer;
  padding: 0.5em 1em;
  margin: 10px 0;
  font-weight: 700;
  font-size: 1.25em;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  :hover {
    opacity: 1 !important;
    box-shadow: 0 1em 1em #2c2c2c07;
  }
  @media (max-width: 450px) {
    padding: 0.5em 1.5em;
    margin: 0;
  }
`
export const ScheduleButtonDark = styled.button`
  color: #fefefe;
  border-color: #fefefe;
  border-width: 0.15em;
  border-style: solid;
  border-radius: 0.2em;
  background: none;
  cursor: pointer;
  padding: 0.5em 1em;
  margin: 10px 0;
  font-weight: 600;
  font-size: 1.25em;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  :hover {
    opacity: 1 !important;
    box-shadow: 0 1em 1em #2c2c2c07;
  }
  @media (max-width: 450px) {
    padding: 0.5em 1.5em;
    margin: 0;
  }
`
export const DownloadButtonDarkArrow = styled.button`
  color: #124e5d;
  border-color: #fefefe;
  border-width: 0.15em;
  border-style: solid;
  border-radius: 0.2em;
  background: #fefefe;
  cursor: pointer;
  padding: 0.5em 1em;
  margin: 10px 0;
  font-weight: 800;
  font-size: 1.25em;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  :hover {
    opacity: 1 !important;
    box-shadow: 0 1em 1em #2c2c2c07;
  }

  @media (max-width: 450px) {
    padding: 0.5em 1.5em;
    margin: 0;
  }
  :after {
    content: "";
    display: inline-block;
    margin-top: 0px;
    margin-left: 4px;
    width: 12px;
    height: 12px;
    border-top: 3.2px solid #124e5d;
    border-right: 3.2px solid #124e5d;
    -moz-transform: rotate(45deg);
    -webkit-transform: rotate(45deg);
    transform: rotate(45deg);
  }
`
export const DownloadButtonArrow = styled.button`
  color: #fefefe;
  border-color: #febd5590;
  border-width: 0.15em;
  border-style: solid;
  border-radius: 0.2em;
  background: #febd55;
  cursor: pointer;
  padding: 0.5em 1em;
  margin: 10px 0;
  font-weight: 800;
  font-size: 1.25em;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  :hover {
    opacity: 1 !important;
    box-shadow: 0 1em 1em #2c2c2c07;
  }

  @media (max-width: 450px) {
    padding: 0.5em 1.5em;
    margin: 0;
  }
  :after {
    content: "";
    display: inline-block;
    margin-top: 0px;
    margin-left: 4px;
    width: 12px;
    height: 12px;
    border-top: 3.2px solid #fefefe;
    border-right: 3.2px solid #fefefe;
    -moz-transform: rotate(45deg);
    -webkit-transform: rotate(45deg);
    transform: rotate(45deg);
  }
`
export const DownloadButton = styled.button`
  color: #fefefe;
  border-color: #febd5590;
  border-width: 0.15em;
  border-style: solid;
  border-radius: 0.2em;
  background: #febd55;
  cursor: pointer;
  padding: 0.5em 1em;
  margin: 10px 0;
  font-weight: 600;
  font-size: 1.25em;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  :hover {
    opacity: 1 !important;
    box-shadow: 0 1em 1em #2c2c2c07;
  }

  @media (max-width: 450px) {
    padding: 0.5em 2em;
    margin: 0;
  }
`
export const DownloadButtonDark = styled.button`
  color: #124e5d;
  border-color: #fefefe;
  border-width: 0.15em;
  border-style: solid;
  border-radius: 0.2em;
  background: #fefefe;
  cursor: pointer;
  padding: 0.5em 1em;
  margin: 10px 0;
  font-weight: 800;
  font-size: 1.25em;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  :hover {
    opacity: 1 !important;
    box-shadow: 0 1em 1em #2c2c2c07;
  }

  @media (max-width: 450px) {
    padding: 0.5em 2em;
    margin: 0;
  }
`
export const RoundButtonUp = styled.button`
  width: 56px;
  height: 56px;
  border: 4px solid #fefefe;
  background-color: #124e5d;
  border-radius: 50%;
  display: inline-block;
  position: fixed;
  bottom: 20px;
  left: 20px;
  cursor: pointer;

  box-shadow: 0 5px 10px #ccc;

  :after {
    content: "U+02191";
    display: inline-block;
    margin-top: 12px;
    margin-left: 0px;
    width: 16px;
    height: 16px;
    border-top: 4px solid #fefefe;
    border-right: 4px solid #fefefe;
    -moz-transform: rotate(-45deg);
    -webkit-transform: rotate(-45deg);
    transform: rotate(-45deg);
  }
  :hover {
    border: 4px solid #124e5d;
    border-top: 4px solid #124e5d;
    border-right: 4px solid #124e5d;
  }
`
export const RoundButtonNext = styled.button`
  width: 56px;
  height: 56px;
  border: 4px solid #b8cace;
  background-color: #fefefe;
  border-radius: 50%;
  display: inline-block;

  :after {
    content: "";
    display: inline-block;
    margin-top: 8px;
    margin-left: -6px;
    width: 16px;
    height: 16px;
    border-top: 4px solid #b8cace;
    border-right: 4px solid #b8cace;
    -moz-transform: rotate(45deg);
    -webkit-transform: rotate(45deg);
    transform: rotate(45deg);
  }
  :hover {
    border: 4px solid #124e5d;
    border-top: 4px solid #124e5d;
    border-right: 4px solid #124e5d;
  }
`
export const RoundButtonDiagonalArrow = styled.button`
  width: 40px;
  height: 40px;
  border: 2px solid #2c2c2c;
  background-color: #fefefe;
  border-radius: 50%;
  display: inline-block;

  :after {
    content: "➝";
    display: inline-block;

    -moz-transform: rotate(-45deg);
    -webkit-transform: rotate(-45deg);
    transform: rotate(-45deg);
  }
  :hover {
    background-color: #124e5d;
    color: #fefefe;
    border: 4px solid #124e5d;
    border-top: 4px solid #124e5d;
    border-right: 4px solid #124e5d;
  }
`
export const ArrowButton = styled.button`
  color: #febd55;
  background-color: #fefefe;
  margin: 1em 0;
  padding: 8px 12px 8px 0px;

  &::after {
    display: inline-block;
    padding-left: 8px;
    content: "➝"; // arrow right unicode
    -webkit-transition: transform 0.3s ease-out;
    -moz-transition: transform 0.3s ease-out;
    -ms-transition: transform 0.3s ease-out;
    -o-transition: transform 0.3s ease-out;
    transition: transform 0.3s ease-out;
  }

  &:hover {
    color: #124e5d;

    &::after {
      -webkit-transform: translateX(4px);
      -moz-transform: translateX(4px);
      -ms-transform: translateX(4px);
      -o-transform: translateX(4px);
      transform: translateX(4px);
    }
  }
`
export const RoundButtonPrev = styled.button`
  width: 56px;
  height: 56px;
  border: 4px solid #b8cace;
  background-color: #fefefe;
  border-radius: 50%;
  display: inline-block;
  :after {
    content: "";
    display: inline-block;
    margin-top: 8px;
    margin-left: 8px;
    width: 16px;
    height: 16px;
    border-top: 4px solid #b8cace;
    border-right: 4px solid #b8cace;
    -moz-transform: rotate(-135deg);
    -webkit-transform: rotate(-135deg);
    transform: rotate(-135deg);
  }
  :hover {
    border: 4px solid #124e5d;
    border-top: 4px solid #124e5d;
    border-right: 4px solid #124e5d;
  }
`
export const ButtonChip = styled.button`
  padding: 4px 8px;
  margin: 0;
  border-color: #b8b8b8;
  border-width: 0.15em;
  border-style: solid;
  border-radius: 12px;
  background: #fefefe;
  font-size: 12px;
  font-weight: 400;
`
