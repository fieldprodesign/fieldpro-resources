import React from "react"
import styled from "styled-components"
import { DownloadButtonArrow, ScheduleButton } from "../buttons/CtaButton.js"
import { Caption } from "../styles/TextStyles.js"
import { Link } from "gatsby"
function LightCtaES(props) {
  return (
    <SectionWrapper>
      <Link to="https://es.fieldproapp.com/book-a-demo">
        <ScheduleButton id="bookdemo">Reserva una demo</ScheduleButton>
      </Link>
      <TrialGroup>
        <a
          href="https://web.v3.fieldproapp.com/welcome"
          target="_blank"
          rel="noreferrer"
        >
          <DownloadButtonArrow id="start_trial">
            Empieza la prueba gratis
          </DownloadButtonArrow>
        </a>
        <DisclaimerTextDark>
          {" "}
          *No se requiere tarjeta de crédito
        </DisclaimerTextDark>
      </TrialGroup>
    </SectionWrapper>
  )
}
export default LightCtaES
const SectionWrapper = styled.div`

display: flex;
  flex-direction: row;
  grid-gap: 20px;
  @media (max-width: 512px) {
    flex-direction: column;
  }
`

const TrialGroup = styled.div``
const DisclaimerTextDark = styled(Caption)`
  color: #6c6c6c;
  padding: 4px 0 0 16px;
  font-weight: 600;
  font-size: 14px;
`
