import React from "react"
import styled from "styled-components"
import {
  DownloadButtonDarkArrow,
  ScheduleButtonDark,
} from "../buttons/CtaButton.js"
import { Caption } from "../styles/TextStyles.js"
import { Link } from "gatsby"
function DarkCtaFR(props) {
  return (
    <SectionWrapper>
      <Link to="https://fr.fieldproapp.com/book-a-demo">
        <ScheduleButtonDark id="bookdemo">Demander une démo</ScheduleButtonDark>
      </Link>
      <TrialGroup>
        <a
          href="https://web.v3.fieldproapp.com/welcome"
          target="_blank"
          rel="noreferrer"
        >
          <DownloadButtonDarkArrow id="start_trial">
            Essai gratuit
          </DownloadButtonDarkArrow>
        </a>

        <DisclaimerText> *Sans engagement</DisclaimerText>
      </TrialGroup>
    </SectionWrapper>
  )
}
export default DarkCtaFR
const SectionWrapper = styled.div`

display: flex;
  flex-direction: row;
  grid-gap: 20px;
  @media (max-width: 512px) {
    flex-direction: column;
  }
`

const TrialGroup = styled.div``
const DisclaimerText = styled(Caption)`
  color: #fefefe;
  padding: 4px 0 0 16px;
  font-weight: 600;
  font-size: 14px;
`
