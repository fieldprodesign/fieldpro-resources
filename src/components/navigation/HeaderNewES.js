import React, { useEffect, useRef, useState } from "react"

import { Link } from "gatsby"
import { StaticQuery, graphql } from "gatsby"
import { documentToReactComponents } from "@contentful/rich-text-react-renderer"


import { HeaderGroup } from "../styles/TextStyles.js"
import {
  HeaderContainer,
  HeaderContentWrapper,
  LogoContainer,
  DropDownContainer,
  DropDownButton,
  MenuItemsContainer,
  WhyMoreIcon,
  WhyLessIcon,
  WhyDropDown,
  DropDownGroup,
  WhyFieldProMenuGroup,
  ForyourIndustryContainer,
  MenuSectionTitle,
  HorizontalSeparator,
  ForyourWorkflowContainer,
  FeatureMoreIcon,
  FeatureLessIcon,
  FeatureDropDown,
  MenuRow,
  MenuCard,
  IconPicture,
  MenuText,
  TitleText,
  DescText,
  MenuItem,
  ResourcesMoreIcon,
  ResourcesLessIcon,
  ResourcesDropDown,
  LoginContainer,
  MenuLangBtns,
  DropDownLangContainer,
  DropDownLangButton,
  LangDropDown,
  DropDownLangGroup,
  LanguageOption,
  SeparatorContainer,
  Separator,
  DropDownLabel,
  ClientsMoreIcon,
  ClientsLessIcon,
  ClientsDropDown,
  CTAContainer,
  MenuCTAbutton,
  MenuIconContainer,
  MenuIcon,
} from "../navigation/HeaderNew.js"

function NewHeaderES(props) {
  const [WhyIsOpen, setWhyIsOpen] = useState(false)
  const [FeatureIsOpen, setFeatureIsOpen] = useState(false)
  const [ClientsIsOpen, setClientsIsOpen] = useState(false)
  const [ResourcesIsOpen, setResourcesIsOpen] = useState(false)
  const [langdropdownIsOpen, setlangdropdownIsOpen] = useState(false)
  const [menuOpen, toggleMenuOpen] = useState(false)

  const [boxShadow, setBoxShadow] = useState(false)
  const navRef = useRef()
  navRef.current = boxShadow
  useEffect(() => {
    const handleScroll = () => {
      const show = window.scrollY > 50
      if (navRef.current !== show) {
        setBoxShadow(show)
      }
    }
    document.addEventListener("scroll", handleScroll)

    return () => {
      document.removeEventListener("scroll", handleScroll)
    }
  }, [])
  return (
    <StaticQuery
      query={graphql`
        {
          allContentfulFeaturesPage(filter: { language: { eq: "ES" } }) {
            edges {
              node {
                slug
                pageTitleAsSeenOnMenu
                pageShortDescriptionAsSeenOnMenu
                pageIconAsSeenOnMenu {
                  file {
                    url
                  }
                }
              }
            }
          }
          allContentfulIndustryPage(filter: { language: { eq: "ES" } }) {
            edges {
              node {
                slug
                pageTitleAsSeenOnMenu
              }
            }
          }
          allContentfulProductPage(filter: { language: { eq: "ES" } }) {
            edges {
              node {
                slug
                pageTitleAsSeenOnMenu
              }
            }
          }
          allContentfulNewMenu(filter: { language: { eq: "ES" } }) {
            edges {
              node {
                menuItemFive {
                  raw
                }
                menuItemFour {
                  raw
                }
                menuFourSubitem1 {
                  raw
                }
                menuFourSubitem2 {
                  raw
                }
                menuFourSubitem3 {
                  raw
                }
                menuFourSubitem4 {
                  raw
                }
                menuFourSubitem5 {
                  raw
                }
                menuFourSubitem6 {
                  raw
                }
                menuItemOne {
                  raw
                }
                menuItemSix {
                  raw
                }
                menuItemThree {
                  raw
                }
                menuSixSubitems {
                  raw
                }
                menuItemTwo {
                  raw
                }
              }
            }
          }
        }
      `}
      render={data => (
        <HeaderGroup>
          <HeaderContainer boxShadow={boxShadow}>
            <HeaderContentWrapper>
              <LogoContainer>
                <Link
                  target="_parent"
                  activeClassName="active"
                  to="https://es.fieldproapp.com/"
              
                >
                  <img
                    src="/images/FieldPro Logo Small.svg"
                    alt="logo"
                  />
                </Link>
              </LogoContainer>

              <MenuItemsContainer menuOpen={menuOpen}>
                <DropDownContainer>
                  <DropDownButton
                    onClick={() => {
                      setWhyIsOpen(!WhyIsOpen)
                      setFeatureIsOpen(false)
                      setClientsIsOpen(false)
                      setResourcesIsOpen(false)
                    }}
                  >
                    <DropDownLabel>
                    ¿Por qué FieldPro?
                    </DropDownLabel>
                    <WhyMoreIcon
                      WhyIsOpen={WhyIsOpen}
                      src="/images/expand_more.png"
                    />
                    <WhyLessIcon
                      WhyIsOpen={WhyIsOpen}
                      src="/images/expand_less.png"
                    />
                  </DropDownButton>
                  <WhyDropDown WhyIsOpen={WhyIsOpen}>
                    <DropDownGroup>
                      <WhyFieldProMenuGroup>
                        <ForyourIndustryContainer>
                          <MenuSectionTitle>
                            {" "}
                            PARA SU SECTOR
                          </MenuSectionTitle>

                          <HorizontalSeparator></HorizontalSeparator>
                          


                          <Link
                              
                                  target="_parent"
                                  activeClassName="active"
                                 
                                  class="darkGrey"
                              
                                  to="https://es.www.fieldproapp.com/why-fieldpro/solution/fieldpro-sales"
                             
                                >
                                  FieldPro Ventas 
                                </Link>
                             
                                <Link
                                 activeClassName="active"
                                 
                                 class="darkGrey"
                                  target="_parent"
                                  to="https://es.www.fieldproapp.com/why-fieldpro/solution/fieldpro-agent-network"
                                >
   FieldPro Red de Agentes                            </Link>
                             
                                <Link
                                 activeClassName="active"
                                 
                                 class="darkGrey"
                                  target="_parent"
                                  to="https://es.www.fieldproapp.com/why-fieldpro/solution/fieldpro-service"
                                >
FieldPro Servicios                           </Link>
                                <Link
                                 activeClassName="active"
                                 
                                 class="darkGrey"
                                  target="_parent"
                                  to="https://es.www.fieldproapp.com/why-fieldpro/solution/fieldpro-agri-sourcing"
                                >
FieldPro Abastecimiento Agrícola                              </Link>
<Link
                                  target="_blank"
                                  activeClassName="active"
                                  to="https://es.www.fieldproapp.com/detect/home"
                                  class="darkGrey"
                                >
FieldPro Detect                             </Link>
                           
                        </ForyourIndustryContainer>
                        <ForyourWorkflowContainer>
                          {/* <MenuSectionTitle> 
                          PARA SU CASO DE USO
                          </MenuSectionTitle>

                          <HorizontalSeparator></HorizontalSeparator>


                              <Link
                                target="_parent"
                                activeClassName="active"
                                to="https://es.www.fieldproapp.com/why-fieldpro/use-case/sales-force-automation"
                                class="darkGrey"
                              >
                                Automatización de la fuerza de ventas
                              </Link>
                              <Link
                                target="_parent"
                                activeClassName="active"
                                to="https://es.www.fieldproapp.com/why-fieldpro/use-case/retail-execution"
                                class="darkGrey"
                              >
                                                      
Ejecución minorista
                              </Link>
                              <Link
                                target="_parent"
                                activeClassName="active"
                                to="https://es.www.fieldproapp.com/why-fieldpro/use-case/mobile-inspection"
                                class="darkGrey"
                              >
                               Inspección móvil
                              </Link>
                              <Link
                                target="_parent"
                                activeClassName="active"
                                to="https://es.www.fieldproapp.com/why-fieldpro/use-case/field-service-management"
                                class="darkGrey"
                              >
                                Field Service Management
                              </Link> */}
                         
                        </ForyourWorkflowContainer>
                      </WhyFieldProMenuGroup>
                    </DropDownGroup>
                  </WhyDropDown>
                </DropDownContainer>
                <DropDownContainer>
                  <DropDownButton
                    onClick={() => {
                      setFeatureIsOpen(!FeatureIsOpen)

                      setClientsIsOpen(false)
                      setResourcesIsOpen(false)
                      setWhyIsOpen(false)
                    }}
                  >
                    <DropDownLabel>
                    Características
                    </DropDownLabel>
                    <FeatureMoreIcon
                      FeatureIsOpen={FeatureIsOpen}
                      src="/images/expand_more.png"
                    />
                    <FeatureLessIcon
                      FeatureIsOpen={FeatureIsOpen}
                      src="/images/expand_less.png"
                    />
                  </DropDownButton>
                  <FeatureDropDown FeatureIsOpen={FeatureIsOpen}>
                    <DropDownGroup>
                      <MenuRow>
                            <Link
                              target="_parent"
                              to="https://es.www.fieldproapp.com/why-fieldpro/use-case/mobile-inspection"
                            >
                              <MenuCard>
                                <IconPicture>
                                  <img
                                    src="/images/Services icon 1.svg"
                                    alt="FieldPro"
                                  />
                                </IconPicture>
                                <MenuText>
                                  <TitleText>
                                    {" "}
                                    CRM móvil
                                  </TitleText>
                                  <DescText>
                                  Identifique y gestione fácilmente las relaciones con sus clientes
                                  </DescText>
                                </MenuText>
                              </MenuCard>
                            </Link>

                            <Link
                              target="_parent"
                              to="https://es.www.fieldproapp.com/why-fieldpro/use-case/mobile-inspection"
                            >
                              <MenuCard>
                                <IconPicture>
                                  <img
                                      src="/images/Services icon 1.svg"
                                    alt="FieldPro"
                                  />
                                </IconPicture>
                                <MenuText>
                                  <TitleText>
                                    {" "}
                                    Aplicación móvil
                                  </TitleText>
                                  <DescText>
                                  Proporcione a sus usuarios una aplicación móvil sin conexión y fácil de usar
                                  </DescText>
                                </MenuText>
                              </MenuCard>
                            </Link>
 
                            <Link
                              target="_parent"
                              to="https://es.www.fieldproapp.com/features/workflow-builder"
                            >
                              <MenuCard>
                                <IconPicture>
                                  <img
                                        src="/images/Services icon 1.svg"
                                    alt="FieldPro"
                                  />
                                </IconPicture>
                                <MenuText>
                                  <TitleText>
                                    {" "}
                                  
Generador de Flujos de Trabajo
                                  </TitleText>
                                  <DescText>
                                  Establezca una cadena de actividades para digitalizar sus procesos en campo
                                  </DescText>
                                </MenuText>
                              </MenuCard>
                            </Link>
                            <Link
                              target="_parent"
                              to="https://es.www.fieldproapp.com/features/real-time-dashboards"
                            >
                              <MenuCard>
                                <IconPicture>
                                  <img
                                         src="/images/Services icon 1.svg"
                                    alt="FieldPro"
                                  />
                                </IconPicture>
                                <MenuText>
                                  <TitleText>
                                    {" "}
                                    Paneles en tiempo real

                                  </TitleText>
                                  <DescText>
                                  Descubra cómo nuestros clientes están realizando grandes cambios.
                                  </DescText>
                                </MenuText>
                              </MenuCard>
                            </Link>
                            <Link
                              target="_parent"
                              to="https://es.www.fieldproapp.com/features/field-team-management"
                            >
                              <MenuCard>
                                <IconPicture>
                                  <img
                                       src="/images/Services icon 1.svg"
                                    alt="FieldPro"
                                  />
                                </IconPicture>
                                <MenuText>
                                  <TitleText>
                                    {" "}
                                                        





Gestión de equipos de campo

                                  </TitleText>
                                  <DescText>
                                  Haga que sus equipos de campo sean más autónomos y productivos
                                  </DescText>
                                </MenuText>
                              </MenuCard>
                            </Link>
                      </MenuRow>
                    </DropDownGroup>
                  </FeatureDropDown>
                </DropDownContainer>
                <MenuItem>
                  <Link
                    activeClassName="active"
                    to= "https://es.www.fieldproapp.com/pricing"
                     
                  >
                    <div class="darkGrey">
                    Precios
                    </div>
                  </Link>
                </MenuItem>

                <DropDownContainer>
                  <DropDownButton
                    onClick={() => {
                      setResourcesIsOpen(!ResourcesIsOpen)

                      setFeatureIsOpen(false)
                      setClientsIsOpen(false)
                      setWhyIsOpen(false)
                    }}
                  >
                    <DropDownLabel>
                  Recursos
                    </DropDownLabel>
                    <ResourcesMoreIcon
                      ResourcesIsOpen={ResourcesIsOpen}
                      src="/images/expand_more.png"
                    />
                    <ResourcesLessIcon
                      ResourcesIsOpen={ResourcesIsOpen}
                      src="/images/expand_less.png"
                    />
                  </DropDownButton>
                  <ResourcesDropDown ResourcesIsOpen={ResourcesIsOpen}>
                    <DropDownGroup>
                      <Link
                        activeClassName="active"
                        to=
                       "https://resources.fieldproapp.com/add-ons"
                        
                      >
                        <div class="darkGrey">
                        Complementos

                       
                        </div>
                      </Link>
                      <Link
                        activeClassName="active"
                        to=  "https://resources.fieldproapp.com/blog/"
                      >
                        <div class="darkGrey">
                        Blog

                        </div>
                      </Link>
                      <Link
                       
                        to=
                       "https://resources.fieldproapp.com/workflowtemplates/"
                     
                      >
                        <div class="darkGrey">
                       
                       
                        Plantillas de flujo de trabajo

                        </div>
                      </Link>
                      <Link
                        activeClassName="active"
                        to= "https://es.www.fieldproapp.com/become-a-partner"
                      >
                        <div class="darkGrey">
                     
Conviértase en Socio

                        </div>
                      </Link>
                      <Link
                        activeClassName="active"
                        to= "https://es.fieldproapp.com/resources/testimonials"
                      >
                        <div class="darkGrey">
                        Testimonios
                        </div>
                      </Link>
                      <Link
                        activeClassName="active"
                        to= "https://optimetriks.invisionapp.com/dsm/design/optimetriks-brand?mode=preview" 
                      >
                        <div class="darkGrey">
                        Activos de marca
                        </div>
                      </Link>
                    </DropDownGroup>
                  </ResourcesDropDown>
                </DropDownContainer>
              </MenuItemsContainer>

              <LoginContainer menuOpen={menuOpen}>
                <MenuLangBtns>
                  <DropDownLangContainer
                    onClick={() => {
                      setlangdropdownIsOpen(!langdropdownIsOpen)
                    }}
                  >
                    <DropDownLangButton>
                      <img src="/images/World.svg" alt="" />
                    </DropDownLangButton>
                    {langdropdownIsOpen && (
                      <LangDropDown>
                        <DropDownLangGroup>
                          <Link
                            target="_parent"
                            activeClassName="active"
                            to={props.en}
                            class="darkGrey"
                          >
                            <LanguageOption>
                              {" "}
                              <img
                                style={{ padding: "8px 4px 0 0px" }}
                                src="/images/flag-en.png"
                                alt=""
                              />
                              EN
                            </LanguageOption>
                          </Link>
                          <Link
                            target="_parent"
                            activeClassName="active"
                            to={props.fr}
                            class="darkGrey"
                          >
                            <LanguageOption>
                              {" "}
                              <img
                                style={{ padding: "8px 4px 0 0px" }}
                                src="/images/flag-fr.png"
                                alt=""
                              />
                              FR
                            </LanguageOption>
                          </Link>
                          <Link
                            target="_parent"
                            activeClassName="active"
                            to={props.es}
                            class="darkGrey"
                          >
                            <LanguageOption>
                              <img
                                style={{ padding: "8px 4px 0 0px" }}
                                src="/images/flag-es.png"
                                alt=""
                              />
                              ES
                            </LanguageOption>
                          </Link>
                        </DropDownLangGroup>
                      </LangDropDown>
                    )}
                  </DropDownLangContainer>
                </MenuLangBtns>

                <SeparatorContainer>
                  {" "}
                  <Separator></Separator>
                </SeparatorContainer>
                {/* 
                <MenuItem>
                  <Link
                    activeClassName="active"
                    to={
                      data.allContentfulNewMenu.edges[0].node.menuItemFive.raw
                        .content[0].content[1].data.uri
                    }
                  >
                    <div class="darkGrey">
                      {" "}
                      {
                        data.allContentfulNewMenu.edges[0].node.menuItemFive
                          .raw.content[0].content[1].content[0].value
                      }
                    </div>
                  </Link>
                </MenuItem> */}
                <DropDownContainer>
                  <DropDownButton
                    onClick={() => {
                      setClientsIsOpen(!ClientsIsOpen)

                      setFeatureIsOpen(false)
                      setResourcesIsOpen(false)
                      setWhyIsOpen(false)
                    }}
                  >
                    <DropDownLabel>
                    Para los clientes

                   

                    </DropDownLabel>
                    <ClientsMoreIcon
                      ClientsIsOpen={ClientsIsOpen}
                      src="/images/expand_more.png"
                    />
                    <ClientsLessIcon
                      ClientsIsOpen={ClientsIsOpen}
                      src="/images/expand_less.png"
                    />
                  </DropDownButton>
                  <ClientsDropDown ClientsIsOpen={ClientsIsOpen}>
                    <DropDownGroup>
                
                     
                   
                 
                    </DropDownGroup>
                  </ClientsDropDown>
                </DropDownContainer>
                <CTAContainer>
                  {" "}
                  <a
                    href="https://web.v3.fieldproapp.com/welcome"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <MenuCTAbutton id="start_trial">
                      Empieza la prueba gratis
                    </MenuCTAbutton>
                  </a>
                </CTAContainer>
              </LoginContainer>
            </HeaderContentWrapper>
            <MenuIconContainer>
              <MenuIcon
                menuOpen={menuOpen}
                onClick={() => toggleMenuOpen(!menuOpen)}
              >
                <div />
                <div />
                <div />
              </MenuIcon>
            </MenuIconContainer>
          </HeaderContainer>
        </HeaderGroup>
      )}
    ></StaticQuery>
  )
}

export default NewHeaderES
