import React from "react"
import { Link } from "gatsby"

import Layout from "../layout.js"
import { H3, P } from "../styles/TextStyles.js"
import {
  FooterContainer,
  FooterGroup,
  FooterLogo,
  LinkGroup,
  Copyright,
  BottomFooterGroup,
  SocialIconsGroup,
  SocialIcon,
} from "../navigation/Footer.js"

const FooterFR = () => {
  return (
    <Layout>
      <FooterContainer>
        <FooterGroup>
          <FooterLogo>
            <Link to="https://fieldproapp.com/fr">
              <img
                   src="/images/optimetriks-logo.png"
                alt=""
              />
            </Link>
          </FooterLogo>
  
          <LinkGroup>
          <H3>Solutions</H3>
          





          <Link to="https://fr.fieldproapp.com/why-fieldpro/solution/fieldpro-sales"> FieldPro Sales</Link>
          <Link to="https://fr.fieldproapp.com/why-fieldpro/solution/fieldpro-service">            
          FieldPro Service</Link>
          <Link to="https://fr.fieldproapp.com/why-fieldpro/solution/fieldpro-agent-network">FieldPro Agent Network</Link>
          
          <Link to="https://fr.fieldproapp.com/why-fieldpro/solution/fieldpro-agri-sourcing">FieldPro Agri-sourcing</Link>
          <Link to="https://fr.fieldproapp.com/detect/home">FieldPro Detect </Link>

          </LinkGroup>
          <LinkGroup>
          <H3>Fonctionnalités</H3>
          





          <Link to="https://fr.fieldproapp.com/features/mobile-crm">   CRM mobile</Link>
          <Link to="https://fr.fieldproapp.com/features/mobile-app">            
Application mobile</Link>
          <Link to="https://fr.fieldproapp.com/features/workflow-builder"> Editeur de workflow no code</Link>
          
          <Link to="https://fr.fieldproapp.com/features/field-team-management">Gestion des équipes terrain </Link>
          <Link to="https://fr.fieldproapp.com/features/real-time-dashboards">Tableaux de bord en temps réel </Link>

          </LinkGroup>
          <LinkGroup>
            <H3>Resources</H3>
            <Link to="https://fr.fieldproapp.com/resources/testimonials">Témoignages</Link>
            <Link to="https://fr.fieldproapp.com/resources/blog">Cas clients</Link>
            <Link to="https://fr.fieldproapp.com/resources/blog">Blog</Link>
            <Link to="https://fr.fieldproapp.com/become-a-partner">Devenir partenaire</Link>
           



            <Link to="https://fr.fieldproapp.com/resources/worflow-templates">Workflows modèles</Link>
            <Link
              target="_blank"
              to="https://optimetriks.invisionapp.com/dsm/design/optimetriks-brand"
            >
              Brand Assets
            </Link>
          </LinkGroup>
          <LinkGroup>
            <H3>Entreprise</H3>
            <Link to="https://fr.fieldproapp.com/about-us">A propos de nous</Link>
         
            <Link to="https://fr.fieldproapp.com/contact-us">Nous-contacter</Link>
            <Link to="https://optimetriks.factorialhr.com/">Recrutement</Link>
            <Link to="https://fr.fieldproapp.com/privacy-policy">Politique de Confidentialité</Link>
            <Link to="https://fr.fieldproapp.com/terms-and-conditions">Conditions Générales d'Utilisation</Link>
          </LinkGroup>
          <LinkGroup>
            <H3>Clients</H3>
            <Link to="https://web.fieldproapp.com/">
            Portail Web
            </Link>
            <Link to="https://learn.fieldproapp.com/fr/">
              Centre d'aide
            </Link>
          </LinkGroup>
        </FooterGroup>

        <BottomFooterGroup>
          <Copyright>
            <P>
              © {new Date().getFullYear()} | Optimetriks | Tous droits réservés{" "}
            </P>
          </Copyright>

          <SocialIconsGroup>
            {" "}
            <a
              href="https://play.google.com/store/apps/details?id=com.optimetriks.smala&hl=en&gl=US"
              target="_blank"
              rel="noopener noreferrer"
            >
              <SocialIcon src="/images/google-play.svg" alt="Playstore Logo" />
            </a>
            <a
              href="https://www.linkedin.com/company/optimetriks"
              target="_blank"
              rel="noopener noreferrer"
            >
              <SocialIcon src="/images/linkedin.svg" alt="LinkedIn Logo" />
            </a>
            <a
              href="https://www.facebook.com/fieldproapp"
              target="_blank"
              rel="noopener noreferrer"
            >
              <SocialIcon src="/images/facebook.svg" alt="Facebook Logo" />
            </a>
            <a
              href="https://www.youtube.com/channel/UC3lznqy3g-OCcUvci_FkVzg"
              target="_blank"
              rel="noopener noreferrer"
            >
              <SocialIcon src="/images/Play button.svg" alt="Youtube Logo" />
            </a>
            <a
              href="https://twitter.com/optimetriks"
              target="_blank"
              rel="noopener noreferrer"
            >
              <SocialIcon src="/images/twitter.svg" alt="Twitter Logo" />
            </a>
          </SocialIconsGroup>
        </BottomFooterGroup>
      </FooterContainer>
    </Layout>
  )
}

export default FooterFR
