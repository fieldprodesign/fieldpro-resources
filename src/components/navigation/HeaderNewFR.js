import React, { useEffect, useRef, useState } from "react"

import { Link } from "gatsby"
import { StaticQuery, graphql } from "gatsby"
import { documentToReactComponents } from "@contentful/rich-text-react-renderer"


import { HeaderGroup } from "../styles/TextStyles.js"

import {
  HeaderContainer,
  HeaderContentWrapper,
  LogoContainer,
  DropDownContainer,
  DropDownButton,
  MenuItemsContainer,
  WhyMoreIcon,
  WhyLessIcon,
  WhyDropDown,
  DropDownGroup,
  WhyFieldProMenuGroup,
  ForyourIndustryContainer,
  MenuSectionTitle,
  HorizontalSeparator,
  ForyourWorkflowContainer,
  FeatureMoreIcon,
  FeatureLessIcon,
  FeatureDropDown,
  MenuRow,
  MenuCard,
  IconPicture,
  MenuText,
  TitleText,
  DescText,
  MenuItem,
  ResourcesMoreIcon,
  ResourcesLessIcon,
  ResourcesDropDown,
  LoginContainer,
  MenuLangBtns,
  DropDownLangContainer,
  DropDownLangButton,
  LangDropDown,
  DropDownLangGroup,
  LanguageOption,
  SeparatorContainer,
  Separator,
  DropDownLabel,
  ClientsMoreIcon,
  ClientsLessIcon,
  ClientsDropDown,
  CTAContainer,
  MenuCTAbutton,
  MenuIconContainer,
  MenuIcon,
} from "../navigation/HeaderNew.js"

function NewHeaderFR(props) {
  const [WhyIsOpen, setWhyIsOpen] = useState(false)
  const [FeatureIsOpen, setFeatureIsOpen] = useState(false)
  const [ClientsIsOpen, setClientsIsOpen] = useState(false)
  const [ResourcesIsOpen, setResourcesIsOpen] = useState(false)
  const [langdropdownIsOpen, setlangdropdownIsOpen] = useState(false)
  const [menuOpen, toggleMenuOpen] = useState(false)

  const [boxShadow, setBoxShadow] = useState(false)
  const navRef = useRef()
  navRef.current = boxShadow
  useEffect(() => {
    const handleScroll = () => {
      const show = window.scrollY > 50
      if (navRef.current !== show) {
        setBoxShadow(show)
      }
    }
    document.addEventListener("scroll", handleScroll)

    return () => {
      document.removeEventListener("scroll", handleScroll)
    }
  }, [])
  return (
    <StaticQuery
      query={graphql`
        {
          allContentfulFeaturesPage(filter: { language: { eq: "FR" } }) {
            edges {
              node {
                slug
                pageTitleAsSeenOnMenu
                pageShortDescriptionAsSeenOnMenu
                pageIconAsSeenOnMenu {
                  file {
                    url
                  }
                }
              }
            }
          }
          allContentfulIndustryPage(filter: { language: { eq: "FR" } }) {
            edges {
              node {
                slug
                pageTitleAsSeenOnMenu
              }
            }
          }
          allContentfulProductPage(filter: { language: { eq: "FR" } }) {
            edges {
              node {
                slug
                pageTitleAsSeenOnMenu
              }
            }
          }
          allContentfulNewMenu(filter: { language: { eq: "FR" } }) {
            edges {
              node {
                menuItemFive {
                  raw
                }
                menuItemFour {
                  raw
                }
                menuFourSubitem1 {
                  raw
                }
                menuFourSubitem2 {
                  raw
                }
                menuFourSubitem3 {
                  raw
                }
                menuFourSubitem4 {
                  raw
                }
                menuFourSubitem5 {
                  raw
                }
                menuFourSubitem6 {
                  raw
                }
                menuItemOne {
                  raw
                }
                menuItemSix {
                  raw
                }
                menuItemThree {
                  raw
                }
                menuSixSubitems {
                  raw
                }
                menuItemTwo {
                  raw
                }
              }
            }
          }
        }
      `}
      render={data => (
        <HeaderGroup>
        <HeaderContainer boxShadow={boxShadow}>
          <HeaderContentWrapper>
            <LogoContainer>
              <Link
                target="_parent"
                activeClassName="active"
                to="https://fr.fieldproapp.com/"
              >
                <img
                  src="/images/FieldPro Logo Small.svg"
                  alt="logo"
                />
              </Link>
            </LogoContainer>

            <MenuItemsContainer menuOpen={menuOpen}>
              <DropDownContainer>
                <DropDownButton
                  onClick={() => {
                    setWhyIsOpen(!WhyIsOpen)
                    setFeatureIsOpen(false)
                    setClientsIsOpen(false)
                    setResourcesIsOpen(false)
                  }}
                >
                  <DropDownLabel>
                  Pourquoi FieldPro ?
                  </DropDownLabel>
                  <WhyMoreIcon
                    WhyIsOpen={WhyIsOpen}
                    src="/images/expand_more.png"
                  />
                  <WhyLessIcon
                    WhyIsOpen={WhyIsOpen}
                    src="/images/expand_less.png"
                  />
                </DropDownButton>
                <WhyDropDown WhyIsOpen={WhyIsOpen}>
                  <DropDownGroup>
                    <WhyFieldProMenuGroup>
                      <ForyourIndustryContainer>
                  
                      
                       
                            <MenuSectionTitle> SOLUTIONS</MenuSectionTitle>

                        <HorizontalSeparator></HorizontalSeparator>
                        






    <Link
      target="_parent"
      activeClassName="active"
      to="https://fr.www.fieldproapp.com/why-fieldpro/solution/fieldpro-sales"
      class="darkGrey"
    >
     FieldPro Sales
    </Link>
  
    <Link
      target="_parent"
      activeClassName="active"
      to="https://fr.www.fieldproapp.com/why-fieldpro/solution/fieldpro-agent-network"
      class="darkGrey"
    >
         FieldPro Agent network 
    </Link>
    <Link
      target="_parent"
      activeClassName="active"
      to="https://fr.www.fieldproapp.com/why-fieldpro/solution/fieldpro-service"
      class="darkGrey"
    >
         FieldPro Service 
    </Link>
    <Link
        target="_parent"
        activeClassName="active"
        to="https://fr.www.fieldproapp.com/why-fieldpro/solution/fieldpro-agri-sourcing"
        class="darkGrey"
      >
      FieldPro Agri Sourcing  
      </Link>
      <Link
                                  target="_blank"
                                  activeClassName="active"
                                  to="https://fr.fieldproapp.com/detect/home"
                                  class="darkGrey"
                                >
FieldPro Detect                             </Link>
                         
                      </ForyourIndustryContainer>
                      <ForyourWorkflowContainer>
                        {/* <MenuSectionTitle> SOLUTIONS</MenuSectionTitle>

                        <HorizontalSeparator></HorizontalSeparator>
                        



                            <Link
                              target="_parent"
                              activeClassName="active"
                              to="https://fr.www.fieldproapp.com/why-fieldpro/use-case/sales-force-automation"
                              class="darkGrey"
                            >
                             Gestion de la force de vente
                            </Link>
                            <Link
                              target="_parent"
                              activeClassName="active"
                              to="https://fr.www.fieldproapp.com/why-fieldpro/use-case/retail-execution"
                              class="darkGrey"
                            >
                         Audit Merchandising
                            </Link>
                            <Link
                              target="_parent"
                              activeClassName="active"
                              to="https://fr.www.fieldproapp.com/why-fieldpro/use-case/mobile-inspection"
                              class="darkGrey"
                            >
                             Inspection technique
                            </Link>
                            <Link
                                target="_parent"
                                activeClassName="active"
                                to="https://fr.www.fieldproapp.com/why-fieldpro/use-case/field-service-management"
                                class="darkGrey"
                              >
                                Field Service Management
                              </Link>
                        */}
                      </ForyourWorkflowContainer>
                    </WhyFieldProMenuGroup>
                  </DropDownGroup>
                </WhyDropDown>
              </DropDownContainer>
              <DropDownContainer>
                <DropDownButton
                  onClick={() => {
                    setFeatureIsOpen(!FeatureIsOpen)

                    setClientsIsOpen(false)
                    setResourcesIsOpen(false)
                    setWhyIsOpen(false)
                  }}
                >
                  <DropDownLabel>
                  Fonctionnalités
                  </DropDownLabel>
                  <FeatureMoreIcon
                    FeatureIsOpen={FeatureIsOpen}
                    src="/images/expand_more.png"
                  />
                  <FeatureLessIcon
                    FeatureIsOpen={FeatureIsOpen}
                    src="/images/expand_less.png"
                  />
                </DropDownButton>
                <FeatureDropDown FeatureIsOpen={FeatureIsOpen}>
                  <DropDownGroup>
                    <MenuRow>

                          <Link
                            target="_parent"
                            to="https://fr.www.fieldproapp.com/why-fieldpro/use-case/mobile-inspection"
                          >
                            <MenuCard>
                              <IconPicture>
                                <img
                                  src="/images/Services icon 1.svg"
                                  alt="FieldPro"
                                />
                              </IconPicture>
                              <MenuText>
                                <TitleText>
                                  {" "}
                                  CRM mobile
                                </TitleText>
                                <DescText>
                                Identifiez vos clients et enregistrez toutes vos interactions
                                </DescText>
                              </MenuText>
                            </MenuCard>
                          </Link>

                          <Link
                            target="_parent"
                            to="https://fr.www.fieldproapp.com/why-fieldpro/use-case/mobile-inspection"
                          >
                            <MenuCard>
                              <IconPicture>
                                <img
                                    src="/images/Services icon 1.svg"
                                  alt="FieldPro"
                                />
                              </IconPicture>
                              <MenuText>
                                <TitleText>
                                  {" "}
                               
                                  Application mobile

                                </TitleText>
                                <DescText>
                                Rendez vos équipes terrain plus autonomes avec une application mobile facile d'utilisation
                                </DescText>
                              </MenuText>
                            </MenuCard>
                          </Link>
                          <Link
                            target="_parent"
                            to="https://fr.www.fieldproapp.com/features/workflow-builder"
                          >
                            <MenuCard>
                              <IconPicture>
                                <img
                                      src="/images/Services icon 1.svg"
                                  alt="FieldPro"
                                />
                              </IconPicture>
                              <MenuText>
                                <TitleText>
                                  {" "}
                                
Editeur de workflow no code
                                </TitleText>
                                <DescText>
                                Structurez votre organisation à travers des workflows qui combinent plusieurs étapes


                                </DescText>
                              </MenuText>
                            </MenuCard>
                          </Link>
                          <Link
                            target="_parent"
                            to="https://fr.www.fieldproapp.com/features/real-time-dashboards"
                          >
                            <MenuCard>
                              <IconPicture>
                                <img
                                       src="/images/Services icon 1.svg"
                                  alt="FieldPro"
                                />
                              </IconPicture>
                              <MenuText>
                                <TitleText>
                                  {" "}
                                 
Tableaux de bord en temps réel

                                </TitleText>
                                <DescText>
                                Analysez votre performance de façon précise
                                </DescText>
                              </MenuText>
                            </MenuCard>
                          </Link>
                          <Link
                            target="_parent"
                            to="https://fr.www.fieldproapp.com/features/field-team-management"
                          >
                            <MenuCard>
                              <IconPicture>
                                <img
                                     src="/images/Services icon 1.svg"
                                  alt="FieldPro"
                                />
                              </IconPicture>
                              <MenuText>
                                <TitleText>
                                  {" "}
                                 
Gestion des équipes de terrain

                                </TitleText>
                                <DescText>
                                Rendez vos équipes terrain plus autonomes et efficaces

                                </DescText>
                              </MenuText>
                            </MenuCard>
                          </Link>
                      






                    </MenuRow>
                  </DropDownGroup>
                </FeatureDropDown>
              </DropDownContainer>
              <MenuItem>
                <Link
                  activeClassName="active"
                  to= "https://fr.fieldproapp.com/pricing"
                   
                >
                  <div class="darkGrey">
                  Prix
                  </div>
                </Link>
              </MenuItem>

              <DropDownContainer>
                <DropDownButton
                  onClick={() => {
                    setResourcesIsOpen(!ResourcesIsOpen)

                    setFeatureIsOpen(false)
                    setClientsIsOpen(false)
                    setWhyIsOpen(false)
                  }}
                >
                  <DropDownLabel>
                Ressources
                  </DropDownLabel>
                  <ResourcesMoreIcon
                    ResourcesIsOpen={ResourcesIsOpen}
                    src="/images/expand_more.png"
                  />
                  <ResourcesLessIcon
                    ResourcesIsOpen={ResourcesIsOpen}
                    src="/images/expand_less.png"
                  />
                </DropDownButton>
                <ResourcesDropDown ResourcesIsOpen={ResourcesIsOpen}>
                  <DropDownGroup>





                    <Link
                      activeClassName="active"
                      to=
                     "https://resources.fieldproapp.com/add-ons-fr"
                      
                    >
                      <div class="darkGrey">
                      Extensions
                      </div>
                    </Link>
                    <Link
                      activeClassName="active"
                      to=  "https://resources.fieldproapp.com/blog-fr/"
                    >
                      <div class="darkGrey">
                       Blog
                      </div>
                    </Link>
                    <Link
                     
                      to=
                     "https://resources.fieldproapp.com/workflowtemplates-fr/"
                   
                    >
                      <div class="darkGrey">
                     
                     
                      Workflows modèles
                      </div>
                    </Link>
                    <Link
                      activeClassName="active"
                      to= "https://fr.www.fieldproapp.com/become-a-partner"
                    >
                      <div class="darkGrey">
                      Devenir partenaire
                      </div>
                    </Link>
                    <Link
                      activeClassName="active"
                      to= "https://fr.fieldproapp.com/resources/testimonials"
                    >
                      <div class="darkGrey">
                      Témoignages
                      </div>
                    </Link>
                    <Link
                      activeClassName="active"
                      to= "https://optimetriks.invisionapp.com/dsm/design/optimetriks-brand?mode=preview" 
                    >
                      <div class="darkGrey">
                      Kit Marketing
                      </div>
                    </Link>
                  </DropDownGroup>
                </ResourcesDropDown>
              </DropDownContainer>
            </MenuItemsContainer>
              <LoginContainer menuOpen={menuOpen}>
                <MenuLangBtns>
                  <DropDownLangContainer
                    onClick={() => {
                      setlangdropdownIsOpen(!langdropdownIsOpen)
                    }}
                  >
                    <DropDownLangButton>
                      <img src="/images/World.svg" alt="" />
                    </DropDownLangButton>
                    {langdropdownIsOpen && (
                      <LangDropDown>
                        <DropDownLangGroup>
                          <Link
                            target="_parent"
                            activeClassName="active"
                            to={props.en}
                            class="darkGrey"
                          >
                            <LanguageOption>
                              {" "}
                              <img
                                style={{ padding: "8px 4px 0 0px" }}
                                src="/images/flag-en.png"
                                alt=""
                              />
                              EN
                            </LanguageOption>
                          </Link>
                          <Link
                            target="_parent"
                            activeClassName="active"
                            to={props.fr}
                            class="darkGrey"
                          >
                            <LanguageOption>
                              {" "}
                              <img
                                style={{ padding: "8px 4px 0 0px" }}
                                src="/images/flag-fr.png"
                                alt=""
                              />
                              FR
                            </LanguageOption>
                          </Link>
                          <Link
                            target="_parent"
                            activeClassName="active"
                            to={props.es}
                            class="darkGrey"
                          >
                            <LanguageOption>
                              <img
                                style={{ padding: "8px 4px 0 0px" }}
                                src="/images/flag-es.png"
                                alt=""
                              />
                              ES
                            </LanguageOption>
                          </Link>
                        </DropDownLangGroup>
                      </LangDropDown>
                    )}
                  </DropDownLangContainer>
                </MenuLangBtns>

                <SeparatorContainer>
                  {" "}
                  <Separator></Separator>
                </SeparatorContainer>

                {/* <MenuItem>
                  <Link
                    activeClassName="active"
                    to={
                      data.allContentfulNewMenu.edges[0].node.menuItemFive.raw
                        .content[0].content[1].data.uri
                    }
                  >
                    <div class="darkGrey">
                      {" "}
                      {
                        data.allContentfulNewMenu.edges[0].node.menuItemFive
                          .raw.content[0].content[1].content[0].value
                      }
                    </div>
                  </Link>
                </MenuItem> */}
                  <DropDownContainer>
                  <DropDownButton
                    onClick={() => {
                      setClientsIsOpen(!ClientsIsOpen)

                      setFeatureIsOpen(false)
                      setResourcesIsOpen(false)
                      setWhyIsOpen(false)
                    }}
                  >
                    <DropDownLabel>
                 Pour les Clients
                    
                    </DropDownLabel>
                    <ClientsMoreIcon
                      ClientsIsOpen={ClientsIsOpen}
                      src="/images/expand_more.png"
                    />
                    <ClientsLessIcon
                      ClientsIsOpen={ClientsIsOpen}
                      src="/images/expand_less.png"
                    />
                  </DropDownButton>
                  <ClientsDropDown ClientsIsOpen={ClientsIsOpen}>
                    <DropDownGroup>
                    <Link
                        activeClassName="active"
                        to= "https://web.fieldproapp.com/"
                      >
                        <div class="darkGrey">
                        Portail Web
                        </div>
                      </Link>
                      <Link
                        activeClassName="active"
                        to= "https://learn.fieldproapp.com/fr/"
                      >
                        <div class="darkGrey">
                        Centre d'aide
                        </div>
                      </Link>
                  
                        
                    
                    </DropDownGroup>
                  </ClientsDropDown>
                </DropDownContainer>
                <CTAContainer>
                  <a
                    href="https://web.v3.fieldproapp.com/welcome"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <MenuCTAbutton id="start_trial">
                      Essai gratuit
                    </MenuCTAbutton>
                  </a>
                </CTAContainer>
              </LoginContainer>
            </HeaderContentWrapper>
            <MenuIconContainer>
              <MenuIcon
                menuOpen={menuOpen}
                onClick={() => toggleMenuOpen(!menuOpen)}
              >
                <div />
                <div />
                <div />
              </MenuIcon>
            </MenuIconContainer>
          </HeaderContainer>
        </HeaderGroup>
      )}
    ></StaticQuery>
  )
}

export default NewHeaderFR
