import React from "react"
import { Link } from "gatsby"

import Layout from "../layout.js"
import { H3, P } from "../styles/TextStyles.js"
import {
  FooterContainer,
  FooterGroup,
  FooterLogo,
  LinkGroup,
  Copyright,
  BottomFooterGroup,
  SocialIconsGroup,
  SocialIcon,
} from "../navigation/Footer.js"

const FooterES = () => {
  return (
    <Layout>
      <FooterContainer>
        <FooterGroup>
          <FooterLogo>
            <Link to="https://fieldproapp.com/es">
              <img
                  src="/images/optimetriks-logo.png"
                alt=""
              />
            </Link>
          </FooterLogo>
          <LinkGroup>
            <H3>Soluciones</H3>
            <Link
                                  target="_parent"
                              
                                  to="https://es.www.fieldproapp.com/why-fieldpro/solution/fieldpro-sales"
                             
                             
                                > 
                                  FieldPro Ventas 
                                </Link>
                                <Link
                                  target="_parent"
                              
                                  to="https://es.www.fieldproapp.com/why-fieldpro/solution/fieldpro-agent-network"
                             
                             
                                > 
                                   FieldPro Red de Agentes
                                </Link>
                                <Link
                                  target="_parent"
                              
                                  to="https://es.www.fieldproapp.com/why-fieldpro/solution/fieldpro-service"
                             
                             
                                > 
                                 FieldPro Servicios
                                </Link>
                              
                                <Link
                                  target="_parent"
                                  to="https://es.www.fieldproapp.com/why-fieldpro/solution/fieldpro-agri-sourcing"
                                >
FieldPro Abastecimiento Agrícola                              </Link><Link
                                  target="_parent"
                                  to="https://es.www.fieldproapp.com/detect/home"
                                >
FieldPro Detect                             </Link>
          </LinkGroup>
        



          <LinkGroup>
            <H3>Características</H3>
          
        







            <Link to="https://es.fieldproapp.com/features/mobile-crm">      CRM móvil</Link>
            <Link to="https://es.fieldproapp.com/features/mobile-app">            
            Aplicación móvil</Link>
            <Link to="https://es.fieldproapp.com/features/workflow-builder"> Generador de flujos de trabajo</Link>
            
            <Link to="https://es.fieldproapp.com/features/field-team-management">Gestión del equipo de campo
</Link>
            <Link to="https://es.fieldproapp.com/features/real-time-dashboards"> Paneles en tiempo real</Link>

          </LinkGroup>
          <LinkGroup>
            <H3>Recursos</H3>
            <Link to="https://es.fieldproapp.com/resources/testimonials">  Testimonios</Link>
            <Link to="https://es.fieldproapp.com/resources/blog">Casos prácticos</Link>
            <Link to="https://es.fieldproapp.com/resources/blog">Blog</Link>
            <Link to="https://es.fieldproapp.com/become-a-partner">Conviértase en Socio</Link>
            {/* <Link to="/find-a-partner-es">Encuentra un socio</Link> */}
            <Link to="https://es.fieldproapp.com/resources/worflow-templates">Plantillas de flujo de trabajo</Link>
            <Link
              target="_blank"
              to="https://optimetriks.invisionapp.com/dsm/design/optimetriks-brand"
            >
            Activos de marca
            </Link>
          </LinkGroup>
          <LinkGroup>
            <H3>Empresa</H3>
            <Link to="https://es.fieldproapp.com/about-us">Quiénes somos</Link>
            <Link to="https://es.fieldproapp.com/contact-us">Póngase en contacto con nosotros</Link>
            <Link to="https://optimetriks.factorialhr.com/">Carreras profesionales</Link>
            <Link to="https://es.fieldproapp.com/privacy-policy">Política de privacidad
</Link>
            <Link to="https://es.fieldproapp.com/terms-and-conditions">
Condiciones generales</Link>
          </LinkGroup>

          <LinkGroup>
            <H3>Para los clientes</H3>
            <Link to="https://web.fieldproapp.com/">Inicio de sesión
</Link>
            <Link to="https://learn.fieldproapp.com/es/">
            Centro de ayuda
            </Link>
          </LinkGroup>
        </FooterGroup>

        <BottomFooterGroup>
          <Copyright>
            <P>
              © {new Date().getFullYear()} | Optimetriks | Todos los derechos
              reservados
            </P>
          </Copyright>

          <SocialIconsGroup>
            {" "}
            <a
              href="https://play.google.com/store/apps/details?id=com.optimetriks.smala&hl=en&gl=US"
              target="_blank"
              rel="noopener noreferrer"
            >
              <SocialIcon src="/images/google-play.svg" alt="Playstore Logo" />
            </a>
            <a
              href="https://www.linkedin.com/company/optimetriks"
              target="_blank"
              rel="noopener noreferrer"
            >
              <SocialIcon src="/images/linkedin.svg" alt="LinkedIn Logo" />
            </a>
            <a
              href="https://www.facebook.com/fieldproapp"
              target="_blank"
              rel="noopener noreferrer"
            >
              <SocialIcon src="/images/facebook.svg" alt="Facebook Logo" />
            </a>
            <a
              href="https://www.youtube.com/channel/UC3lznqy3g-OCcUvci_FkVzg"
              target="_blank"
              rel="noopener noreferrer"
            >
              <SocialIcon src="/images/Play button.svg" alt="Youtube Logo" />
            </a>
            <a
              href="https://twitter.com/optimetriks"
              target="_blank"
              rel="noopener noreferrer"
            >
              <SocialIcon src="/images/twitter.svg" alt="Twitter Logo" />
            </a>
          </SocialIconsGroup>
        </BottomFooterGroup>
      </FooterContainer>
    </Layout>
  )
}

export default FooterES
