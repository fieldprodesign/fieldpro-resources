import React from "react"
import { graphql, Link } from "gatsby"

import Layout from "../components/layout.js"
import Seo from "../components/seo"

import Footer from "../components/navigation/Footer.js"

import { H1, P } from "../components/styles/TextStyles.js"
import styled from "styled-components"
import { DownloadButton } from "../components/buttons/CtaButton.js"
import NewHeader from "../components/navigation/HeaderNew.js"

const NotFoundPage = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title
  //404 PAGE
  return (
    <Layout location={location} title={siteTitle}>
      <Seo title="404: Not Found" />
      <NewHeader />
      <BodyWrapper>
        <FourText>
          <Title>Oops!</Title>
          <Description>
            Sorry, we can't seem to find the page you're looking for.
          </Description>
          <Link to="/">
            <DownloadButton>Go to HomePage</DownloadButton>
          </Link>
        </FourText>
      </BodyWrapper>

      <Footer />
    </Layout>
  )
}

export default NotFoundPage

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }
`
const BodyWrapper = styled.div`
  max-width: 1280px;
  margin: 0 auto;
`
const FourText = styled.div`
  margin: 240px 0 320px 0;
  @media (max-width: 450px) {
    margin: 120px 0 120px 0;
  }
`
const Title = styled(H1)`
  padding-bottom: 20px;
`
const Description = styled(P)``
