import React, { useState, } from "react"
import { graphql } from "gatsby"

import Layout from "../components/layout.js"
import Seo from "../components/seo.js"
import {

 
  HeroSectionWrapper,
  HeroSection,
  HeroTitle,
  HeroDescription,
  SeearchBarContainer,
  SearchInput,
  WorkflowTemplatesWrapper,
  CardTemplateWrapper,
  CardTemplateIcon,
  CardTemplateTitle,
  CardTemplateDescription,
  CardDividerLine,
  CardChipGroup,
  CtaBackground,
  CtaIllustartion,
  CtaWrapper,
  CtaContent,
  CtaTagline,
  CtaButtons,
  FilterSearchBar,
  FilterBar,

} from "../components/styles/WorkflowTemplateStyles.js";
import NewHeaderES from "../components/navigation/HeaderNewES.js"
import FooterES from "../components/navigation/FooterES.js"

import { ButtonChip } from "../components/buttons/CtaButton.js"
import { Link } from "gatsby"
import ScrollToTop from "../components/sections/ScrollToTop.js"
import DarkCtaES from "../components/calltoaction/DarkCtaES.js"

function AppTemplatesES({ data }) {
  const cards = data.allContentfulWorkflowTemplate.edges
  const seoContent = data.allContentfulMetasForGeneratedPages.edges[0]
  const [title, setTitle] = useState("")
  const [foundCards, setFoundCards] = useState(cards)

  const filter = e => {
    const keyword = e.target.value

    if (keyword !== "") {
      const results = cards.filter(workflowcard => {
        return workflowcard.node.title
          .toLowerCase()
          .includes(keyword.toLowerCase())
        // Use the toLowerCase() method to make it case-insensitive
      })
      setFoundCards(results)
    } else {
      setFoundCards(cards)
      // If the text field is empty, show all users
    }

    setTitle(keyword)
  }


  return (
    <Layout>
      <Seo
        title={seoContent.node.workflowsSeoMetaTitle}
        description={seoContent.node.workflowsSeoMetaDescription}
      />
      <NewHeaderES
        en="/workflowtemplates"
        fr="/workflowtemplates-fr"
        es="/workflowtemplates-es"
      />
      <HeroSectionWrapper>
        <HeroSection>
          <HeroTitle>Plantillas de Flujos de Trabajo</HeroTitle>
          <HeroDescription>
            Empieza ahora utilizando cualquiera de nuestras plantillas de flujos
            de trabajo configurables.
          </HeroDescription>
        </HeroSection>
      </HeroSectionWrapper>
      <FilterSearchBar>
        <FilterBar>
          {/* <TextTitle>FilterBy</TextTitle>
          <CategoryBtn>
            <DdButton>Dropdown</DdButton>
            
          </CategoryBtn>
          <ResetBtn></ResetBtn> */}
        </FilterBar>
        <SeearchBarContainer
         
        >
          <SearchInput
            type="search"
            value={title}
            onChange={filter}
            placeholder="Search template title..."
          />
        </SeearchBarContainer>
      </FilterSearchBar>
      <WorkflowTemplatesWrapper>
        {foundCards && foundCards.length > 0 ? (
          foundCards.map(workflowcard => (
            <Link to={`/${workflowcard.node.slug}`} key={workflowcard.node.id}>
              <CardTemplateWrapper>
                <CardTemplateIcon>
                  {<img src={workflowcard.node.workflowIcon.file.url} alt="" />}
                </CardTemplateIcon>
                <CardTemplateTitle>{workflowcard.node.title}</CardTemplateTitle>
                <CardTemplateDescription>
                  {workflowcard.node.metaDescription}
                </CardTemplateDescription>
                <CardDividerLine></CardDividerLine>
                <CardChipGroup>
                  {workflowcard.node.tags.map(templatetags => {
                    return <ButtonChip>{templatetags.title}</ButtonChip>
                  })}
                </CardChipGroup>
              </CardTemplateWrapper>
            </Link>
          ))
        ) : (
          <h1>We are sorry! We couldn't find the results for your search.</h1>
        )}
      </WorkflowTemplatesWrapper>
      <CtaBackground>
        <CtaIllustartion>
          <img
            src="/images/Geo-location.svg"
            style={{ width: "100vw" }}
            alt=""
          />
        </CtaIllustartion>
        <CtaWrapper>
          <CtaContent>
            <CtaTagline>
              {" "}
              {cards.CallToAction || "Manage your field salesforce digitally"}
            </CtaTagline>
            <CtaButtons>
              <DarkCtaES />
            </CtaButtons>
          </CtaContent>
        </CtaWrapper>
      </CtaBackground>
      <FooterES />
      <ScrollToTop />
    </Layout>
  )
}

export const queryES = graphql`
  {
    allContentfulWorkflowTemplate(filter: { language: { eq: "ES" } }) {
      edges {
        node {
          title
          metaDescription
          workflowIcon {
            file {
              url
            }
          }
          tags {
            title
          }
          language
          callToAction
          slug
        }
      }
    }
    allContentfulMetasForGeneratedPages(filter: { language: { eq: "ES" } }) {
      edges {
        node {
          workflowsSeoMetaDescription
          workflowsSeoMetaTitle
        }
      }
    }
  }
`

export default AppTemplatesES
