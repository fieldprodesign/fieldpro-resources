import React from "react"
import { Link, graphql } from "gatsby"

import Layout from "../components/layout.js"
import Seo from "../components/seo.js"

import NewHeaderFR from "../components/navigation/HeaderNewFR.js"
import FooterFR from "../components/navigation/FooterFR.js"

import { HeaderGroup } from "../components/styles/TextStyles.js"
import ScrollToTop from "../components/sections/ScrollToTop.js"
import {

  CategoryMenuContainer,
  CategoryGroup,
  AllCategories,
  Category,
  Body,
  FeaturedPost,
  FeaturedImage,
  FeaturedText,
  FeaturedTitle,
  PostGroup,
  Post,
  PostImage,
  PostText,
  Title,
  CategoryTagContainer,
  CategoryTag,
} from "../components/styles/blogStyles.js";

class BlogFR extends React.Component {
  render() {
    const { data } = this.props
    const seoContent = data.allContentfulMetasForGeneratedPages.edges[0]

    const posts = data.allContentfulBlogPost.edges

    return (
      <Layout location={this.props.location}>
        <Seo
          title={seoContent.node.blogseometatitle}
          description={seoContent.node.blogseometadescription}
        />
        <NewHeaderFR en="/blog" fr="/blog-fr" es="/blog-es" />
        <HeaderGroup>
          <CategoryMenuContainer>
            <CategoryGroup>
              <AllCategories>
                <Link to="/blog-fr" activeClassName="active">
                  All
                </Link>
              </AllCategories>
              {data.allContentfulBlogCategory.edges.map(({ node }) => {
                return (
                  <Category key={node.slug}>
                    <Link activeClassName="active" to={`/${node.slug}`}>
                      {node.title}
                    </Link>
                  </Category>
                )
              })}
            </CategoryGroup>
          </CategoryMenuContainer>
        </HeaderGroup>
        <Body>
          <FeaturedPost>
            {posts.map(({ node }) => {
              const title = node.title || node.slug
              if (node.featured) {
                return (
                  <div key={node.slug}>
                    <FeaturedImage>
                      <Link style={{ boxShadow: `none` }} to={`/${node.slug}`}>
                        <img src={node.image.file.url} alt="" />
                      </Link>
                    </FeaturedImage>
                    <FeaturedText>
                      <FeaturedTitle>
                        <Link
                          style={{ boxShadow: `none` }}
                          to={`/${node.slug}`}
                        >
                          {title}
                        </Link>
                      </FeaturedTitle>
                      <CategoryTagContainer>
                        {node.categories.map(categoryFR => {
                          return (
                            <Link to={`/${categoryFR.slug}`}>
                              <CategoryTag>
                                {categoryFR.title}&#160;&#160;|
                              </CategoryTag>
                            </Link>
                          )
                        })}
                      </CategoryTagContainer>
                    </FeaturedText>
                  </div>
                )
              } else {
                return null
              }
            })}
          </FeaturedPost>
     

       
<PostGroup>
  {posts.map(({ node }) => {
    const title = node.title || node.slug
    if (node.featured) {
      return null
    } else {
      return (
        <Post key={node.slug}>
          <PostImage>
            <Link style={{ boxShadow: `none` }} to={`/${node.slug}`}>
              {node.image && node.image.file && (
                <img src={node.image.file.url} alt="" />
              )}
            </Link>
          </PostImage>
          <PostText>
            <Title>
              <Link style={{ boxShadow: `none` }} to={`/${node.slug}`}>
                {title}
              </Link>
            </Title>
            <CategoryTagContainer>
              {node.categories &&
                node.categories.map(category => {
                  return (
                    <Link to={`/${category.slug}`} key={category.slug}>
                      <CategoryTag>
                        {category.title}&#160;&#160;|
                      </CategoryTag>
                    </Link>
                  )
                })}
            </CategoryTagContainer>
          </PostText>
        </Post>
      )
    }
  })}
</PostGroup>



          
        </Body>
        <FooterFR />
        <ScrollToTop />
      </Layout>
    )
  }
}

export default BlogFR

export const pageQueryFR = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allContentfulBlogCategory(
      sort: {updatedAt: ASC}
      filter: {language: {eq: "FR"}}
    ) {
      edges {
        node {
          title
          slug
        }
      }
    }
    allContentfulBlogPost(sort: {createdAt: DESC}, filter: {language: {eq: "FR"}}) {
      edges {
        node {
          slug
          title
          featured
          image {
            file {
              url
            }
          }
          categories {
            title
            slug
          }
        }
      }
    }
    allContentfulMetasForGeneratedPages(filter: {language: {eq: "FR"}}) {
      edges {
        node {
          blogseometadescription
          blogseometatitle
        }
      }
    }
  }
`
