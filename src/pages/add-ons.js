import React, { useState,  } from "react"
import { graphql } from "gatsby"

import Layout from "../components/layout.js"
import Seo from "../components/seo.js"

import NewHeader from "../components/navigation/HeaderNew.js"
import Footer from "../components/navigation/Footer.js"
import {

  HeroSectionWrapper,HeroSection,HeroTitle,HeroDescription,SeearchBarContainer,SearchInput,WorkflowTemplatesWrapper,CardTemplateWrapper
,CardTemplateIcon,CardTemplateTitle,CardTemplateDescription,CardDividerLine,CardChipGroup,CtaBackground,CtaIllustartion,CtaWrapper,
 CtaContent,CtaTagline,CtaButtons,FilterSearchBar
} from "../components/styles/AddonsStyles.js";

import { ButtonChip } from "../components/buttons/CtaButton.js"
import { Link } from "gatsby"
import ScrollToTop from "../components/sections/ScrollToTop.js"
import DarkCta from "../components/calltoaction/DarkCta.js"

function Integrations({ data }) {
  const cards = data.allContentfulAddOnPage.edges
  const seoContent = data.allContentfulMetasForGeneratedPages.edges[0]
  const [title, setTitle] = useState("")
  const [foundCards, setFoundCards] = useState(cards)

  const filter = e => {
    const keyword = e.target.value

    if (keyword !== "") {
      const results = cards.filter(workflowcard => {
        return workflowcard.node.title
          .toLowerCase()
          .includes(keyword.toLowerCase())
        // Use the toLowerCase() method to make it case-insensitive
      })
      setFoundCards(results)
    } else {
      setFoundCards(cards)
      // If the text field is empty, show all users
    }

    setTitle(keyword)
  }
  
  return (
    <Layout>
      <Seo
        title={seoContent.node.integrationsSeoMetaTitle}
        description={seoContent.node.integrationsSeoMetaDescription}
      />
   
       <NewHeader en="/add-ons" fr="/add-ons-fr" es="/add-ons-es" /> 
      <HeroSectionWrapper>
        <HeroSection >
          <HeroTitle>Add-ons</HeroTitle>
          <HeroDescription>
            Leverage our rich library of add ons and external software
            integrations to adapt FieldPro to your operational needs. Each add
            on and integration is built based on industry best practices and
            will help you get the full value of field work digitisation.
          </HeroDescription>
        </HeroSection>
      </HeroSectionWrapper>
      <FilterSearchBar> 
      <SeearchBarContainer
       
      >
        <SearchInput 
          type="search"
          value={title}
          onChange={filter}
          placeholder="Search add on..."
        />
      </SeearchBarContainer> </FilterSearchBar>
      <WorkflowTemplatesWrapper>
        {foundCards && foundCards.length > 0 ? (
          foundCards.map(workflowcard => (
            <Link to={`/${workflowcard.node.slug}`} key={workflowcard.node.id}>
              <CardTemplateWrapper
                
              >
                <CardTemplateIcon>
                  {
                    <img
                      src={workflowcard.node.integrationIcon.file.url}
                      alt=""
                    />
                  }
                </CardTemplateIcon>
                <CardTemplateTitle>{workflowcard.node.title}</CardTemplateTitle>
                <CardTemplateDescription>
                  {workflowcard.node.metaDescription}
                </CardTemplateDescription>
                <CardDividerLine></CardDividerLine>
                <CardChipGroup>
                  {workflowcard.node.tags.map(templatetags => {
                    return <ButtonChip>{templatetags.title}</ButtonChip>
                  })}
                </CardChipGroup>
              </CardTemplateWrapper>
            </Link>
          ))
        ) : (
          <h1>We are sorry! We couldn't find the results for your search.</h1>
        )}
      </WorkflowTemplatesWrapper>
      <CtaBackground>
        <CtaIllustartion>
          <img
            src="/images/Geo-location.svg"
            style={{
              width: "100vw",
            }}
            alt=""
          />
        </CtaIllustartion>
        <CtaWrapper>
          <CtaContent>
            <CtaTagline>Manage your field salesforce digitally</CtaTagline>
            <CtaButtons>
              <DarkCta />
            </CtaButtons>
          </CtaContent>
        </CtaWrapper>
      </CtaBackground>
      <Footer />
      <ScrollToTop />
    </Layout>
  )
}

export const query = graphql`
  {
    allContentfulAddOnPage(filter: { language: { eq: "EN" } }) {
      edges {
        node {
          callToAction
          metaTitle
          metaDescription
          integrationIcon {
            file {
              url
            }
          }
          tags {
            title
          }
          language
          title
          slug
        }
      }
    }
    allContentfulMetasForGeneratedPages(filter: { language: { eq: "EN" } }) {
      edges {
        node {
          integrationsSeoMetaDescription
          integrationsSeoMetaTitle
        }
      }
    }
  }
`

export default Integrations
